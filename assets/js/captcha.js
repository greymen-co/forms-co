$(function () {
    if (!window.grecaptcha) return;
    var $forms = $('.gm-form');
    if (!$forms.length) return;
    $forms.each(function () {
        var $form = $(this).closest('form');
        var $reCaptchaField = $('[name="g-recaptcha-response"]', $form);
        if (!$reCaptchaField.length) return;
        var $submitButton = $('[type="submit"]', $form);
        if (!$submitButton.length) return;
        $submitButton.on('click', function (e) {
            e.preventDefault();
            window.grecaptcha.ready(function () {
                window.grecaptcha
                    .execute($reCaptchaField.data('site-key'), { action: 'recaptcha' })
                    .then(function (token) {
                        $reCaptchaField.val(token);
                        $form.request();
                    });
            });
        });
    });
});
