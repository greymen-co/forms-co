/* October form validation */
/* old jquery version */
if (typeof jQuery != 'undefined') {
    $(window).on("ajaxInvalidField", function (
        event,
        fieldElement,
        fieldName,
        errorMsg,
        isFirst
    ) {
        event.preventDefault();
        $(fieldElement).addClass("is-invalid");
    });
    
    $(document).on("ajaxPromise", "[data-request]", function () {
        $(this)
            .closest("form")
            .find(".is-invalid")
            .removeClass("is-invalid");
    });    
} else {
    addEventListener('ajax:invalid-field', function(event) {
        const { element, fieldName, errorMsg, isFirst } = event.detail;
        element.classList.add('is-invalid');
    });
    
    addEventListener('ajax:promise', function(event) {
        event.target.closest('form').querySelectorAll('.has-error').forEach(function(el) {
            el.classList.remove('has-error');
        });
    });
    
}
