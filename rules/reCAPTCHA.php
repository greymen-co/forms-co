<?php namespace Greymen\Formsco\Rules;

use Exception;
use Greymen\Formsco\Models\Settings;
use Illuminate\Support\Facades\Http;
use Lang;

class reCAPTCHA
{
    private string $action = 'recaptcha';
    private float $minScore = 0.5;

    public function validate(string $attribute, mixed $value, array $params): bool
    {
        try {
            if (isset($params[0])) {
                $this->action = $params[0];
            }
            if (isset($params[1])) {
                $this->minScore = floatval($params[1]);
            }

            $siteVerify = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
                'secret' => Settings::get('google_recaptcha_secret_key', ''),
                'response' => $value,
            ]);

            if ($siteVerify->failed()) {
                return false;
            }

            if ($siteVerify->successful()) {
                $body = $siteVerify->json();

                // When this fails it means the browser didn't send a correct code. This means it's very likely a bot we should block
                if ($body['success'] !== true) {
                    return false;
                }

                // When this fails it means the action didn't match the one set in the button's data-action.
                // Either a bot or a code mistake. Compare form data-action and value passed to $action (should be equal).
                if ($this->action !== $body['action']) {
                    return false;
                }

                // If we set a minScore threshold, verify that the spam score didn't go below it
                // More info can be found at: https://developers.google.com/recaptcha/docs/v3#interpreting_the_score
                if ($this->minScore > $body['score']) {
                    return false;
                }
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function message(): string
    {
        return Lang::get('greymen.formsco::validation.recaptcha.failed');
    }
}
