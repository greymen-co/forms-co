<?php 
namespace Greymen\Formsco\Components;

use Cms\Classes\ComponentBase;

/**
 * Utmsession Component
 *
 * @link https://docs.octobercms.com/3.x/extend/cms-components.html
 */
class UtmSession extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'utmsession Component',
            'description' => 'Sets UTM to Session ...'
        ];
    }

    /**
     * @link https://docs.octobercms.com/3.x/element/inspector-types.html
     */
    public function defineProperties()
    {
        return [];
    }
    public function onRun()
    {
        // dump('set referer'.request()->headers->get('referer'). ' - '.\Request::server('HTTP_REFERER') );
        // dump(\Session::get('utm'));
        if (input('utm_source'))    \Session::put('utm.source', input('utm_source'));
        if (input('utm_medium'))    \Session::put('utm.medium', input('utm_medium'));
        if (input('utm_campaign'))  \Session::put('utm.campaign', input('utm_campaign'));
        if (input('utm_id'))        \Session::put('utm.id', input('utm_id'));
        if (input('utm_term'))      \Session::put('utm.term', input('utm_term'));
        if (input('utm_content'))   \Session::put('utm.content', input('utm_content'));
        if (input('gclid'))         \Session::put('utm.gclid', input('gclid'));
        if (! \Session::get('utm.referer')) {
            \Session::put('utm.referer', \Request::server('HTTP_REFERER') ? \Request::server('HTTP_REFERER') : 'not set');
        }
        if (input('utm_reset')) \Session::forget('utm');
    }
}
