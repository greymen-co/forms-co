<?php namespace Greymen\Formsco\Components;

use Cms\Classes\ComponentBase;
use Greymen\Formsco\Models\Campaigns;
use Greymen\Formsco\Models\Settings;
use Greymen\Formsco\Models\Subscribers;
use Greymen\Formsco\Classes\ActiveCampaign;

use Http;
use Validator;
use Input;
use ValidationException;
use Lang;
use Mail;
use Event;
use Log;
use File;
use Storage;

class Form extends ComponentBase
{

    var $campaign;
    var $data;
    public function componentDetails()
    {
        return [
            'name'        => 'Form Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'campaign_id' => [
                'title'             => 'Campaign ID',
                'description'       => 'Id of the campaign ',
                'type'              => 'number'
            ],
            'form_id' => [
                'title'             => 'Form ID',
                'description'       => 'ID of the element to send the result to',
                'type'              => 'text'
            ],
            'hide_labels' => [
                'title'             => 'Hide form labels',
                'description'       => 'Hides the labels of a input field',
                'type'              => 'switch'
            ],
            'thankyou_title' => [
                'title'             => 'Form Thank You title',
                'description'       => 'Titel na insturen formulier',
                'type'              => 'text'
            ],
            'thankyou_text' => [
                'title'             => 'Form Thank You text',
                'description'       => 'Tekst onder title na insturen formulier',
                'type'              => 'text'
            ],

        ];
    }
    public function onRender()
    {
        $this->page['campaign_id']      = $this->property('campaign_id');
        $this->page['form_id']          = $this->property('form_id');
        $this->page['thankyou_title']   = $this->property('thankyou_title');
        $this->page['thankyou_text']    = $this->property('thankyou_text');
        $this->page['hide_labels']      = $this->property('hide_labels');
        $this->page['campaign']         = Campaigns::getCampaignById($this->property('campaign_id'), $this);
        $this->page['utm']              = \Session::get('utm');
    }

    public function onRun()
    {
        $this->addJs("/plugins/greymen/formsco/assets/js/validation.js", "1.0.0");
        $this->addCss("/plugins/greymen/formsco/assets/css/validation.css", "1.0.0");

        // Add Google reCAPTCHA
        if (boolval(Settings::get('google_recaptcha_enabled', false))) {
            $this->addJs('//www.google.com/recaptcha/api.js?hl=' . config('app.locale', 'en') . '&render=' . Settings::get('google_recaptcha_site_key', ''));
            $this->addJs('/plugins/greymen/formsco/assets/js/captcha.js?v=' . filemtime(plugins_path('greymen/formsco/assets/js/captcha.js')));
        }
    }
    public function errorMessages()
    {
        $messages   = Lang::get('greymen.formsco::validation');
        $elements   = $this->campaign['data']['form'];
        foreach($elements as $element)
        {
            $rules = explode('|',$element['validation_rules']);
            foreach($rules as $rule)
            {
                $rule = $element['name'] . '.' .explode(':',$rule)[0];
                $messages[$rule] = $element['validation_error_message'];
            }
        }
        return $messages;
    }

    public function onSubmit()
    {
        $this->campaign  = Campaigns::getCampaignById(post('campaign_id'), $this);
        if(!$this->campaign)
        {
            throw new ValidationException('Campaign not found');
        }
        $data   = Input::all();

        $rules  = json_decode(post('rules'),true);

        // Add Google reCAPTCHA validation
        $captchaEnabled = boolval(Settings::get('google_recaptcha_enabled', false));
        if ($captchaEnabled && boolval($this->campaign['data']['enable_captcha'])) {
            $rules['g-recaptcha-response'] = 'required|google_recaptcha';
        }

        $validation = Validator::make($data, $rules, $this->errorMessages($data) );

        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $subscriber                 = new Subscribers();
        $form_data                  = json_encode($data);
        // $subscriber->fill($data);
        // $subscriber->save();
        // unset($data['form_data']);
        unset($data['rules']);
        unset($data['thankyou_title']);
        unset($data['thankyou_text']);
        unset($data['thankyou_text']);
        $data['files']=null;

        foreach(Input::all() as $key => $value) {
            if (Input::hasFile($key)) {
                $uploadsFolder      = Storage::disk("uploads")->getConfig()['root']."/public/";
                $uploadsUrl         = Storage::disk("uploads")->getConfig()['url']."/public/";
                $file               = Input::file($key);
                $fileName           = str_slug(time().'_'.File::name($file->getClientOriginalName()));
                $extension          = strtolower($file->getClientOriginalExtension());
                $url                = url("$uploadsUrl$fileName.$extension");
                $data['files'][]    = [
                                        'url'       => $url,
                                        'path'      => "$uploadsFolder$fileName.$extension",
                                        'filename'  => "$fileName.$extension"
                                    ];
                $file->move($uploadsFolder, "$fileName.$extension");
                $data[$key]     = $url;
            }
        }
        $data['utm']        = \Session::get('utm');
        $data['form_data']  = $form_data;
        $subscriber->fill($data);
        $subscriber->save();

        if (intval($this->campaign['data']['send_data_to_zapier'])===1) $this->sendDataToZapier($data);
        if (intval($this->campaign['data']['send_data_to_mail'])===1) $this->sendConfirmationMail($data);
        if (intval($this->campaign['data']['send_data_to_ac'])===1) $this->sendDataToAc($data);

        Event::fire('subscriber.submit', [$data]);

        return [
            '#' . post('form_id')   => '<h5 >'.post('thankyou_title').'</h5><p>'.post('thankyou_text').'</p>',
            '#modal-btn--send'      => "",
            '.remove-after-send'    => "",
            'success'               => true
        ];
    }

    public function onGetAddress()
    {
        $apiKey = env('POSTNL_API_KEY');
        if (empty($apiKey)) {
            return [];
        }
        $get = [
            'postalCode' => input('PostalCode'),
            'houseNumber' => input('HouseNumber'),
        ];

        $response = Http::withHeaders([
            'apikey' => $apiKey,
            'Content-Type' => 'application/json',
        ])->get('https://api.postnl.nl/v4/address/netherlands', $get);
        if ($response->successful()) {
            $data = $response->json();
            if (!empty($data) && is_array($data)) {
                $data = current($data);
                return [
                    'street' => $data['streetName'],
                    'city' => $data['cityName'],
                ];
            }
        }
        return [];
    }

    private function sendDataToZapier($data)
    {
        $url = $this->campaign['data']['send_data_zapier_url'];
//        $url = 'https://hooks.zapier.com/hooks/catch/4406254/bb9ljhb';
        $this->data = $data;
        $result =  Http::post($url , function($http){
            $http->requestData = $this->data;
            $http->setOption(CURLOPT_SSL_VERIFYHOST, false);
        });
        if($result->code ===200) {
            Log::info('Zapier data send result: '.$result->body);
            return true;
        } else{
            Log::error('Error sending data to Zapier: '.$result->body);
            return false;
        }
    }

    private function sendDataToAc($data)
    {
        $active_campaign = new ActiveCampaign();

        setlocale(LC_ALL, 'nl_NL');
        $c_data = $this->campaign['data'];
        $ac     = [
            // 'email'                             => post('email'),
            "p[" . $c_data['ac_list_id']. "]"   => $c_data['ac_list_id'],
            "tags"                              => $c_data['ac_tag'],
        ];

        foreach($this->campaign['fields'] as $field)
        {
            switch ($field['ac_field_name']) {
                case 'EMAIL':
                    $ac['email'] = $data[$field['name']];
                    break;
                case 'FIRSTNAME':
                    $ac['first_name'] = $data[$field['name']];
                    break;
                case 'LASTNAME':
                    $ac['last_name'] = $data[$field['name']];
                    break;
                case 'PHONE':
                    $ac['phone'] = $data[$field['name']];
                    break;
                default:
                    if($field['ac_field_name'] && isset($data[$field['name']]))
                    {
                        $ac_field       = "field[%".$field['ac_field_name']."%]";
                        $ac[$ac_field]  = $data[$field['name']];
                    }
                    break;
                }
        }

        /* ADD HIDDEN % FIELDS TO AC */
        foreach($data as $key => $value)
        {
            if( substr($key ,0,1)==="%")
                $ac["field[$key]"] = $value;
        }
        $response = $active_campaign->call('contact/sync', $ac);
        return $response;
    }

    private function sendConfirmationMail($data)
    {
        $vars['data'] = $data;

        Mail::send('greymen.formsco::mail.subscribed', $vars, function($message) {
//            $message->from('info@greymen.co', 'website');
            $message->to($this->campaign['data']['send_data_mail_to']);
            $message->subject($this->campaign['data']['send_data_mail_subject']);
        });
    }

}
