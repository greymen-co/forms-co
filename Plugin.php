<?php namespace Greymen\Formsco;

use Cms\Classes\Theme;
use Greymen\Formsco\Models\Settings;
use Greymen\Formsco\Rules\reCAPTCHA;
use RainLab\Pages\Classes\PageList;
use System\Classes\PluginBase;
use Greymen\Formsco\Classes\ActiveCampaign;
use Greymen\Formsco\Models\Campaigns;
use Flash;
use Artisan;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Key Agency Form & Subscribers',
            'description' => 'Subscribe email addresses and passes them on to Active Campaign or MailChimp.',
            'author'      => 'Key',
            'icon'        => 'icon-envelope-square',
            'homepage'    => 'https://keyagency.nl'
        ];
    }

    public function boot()
    {
        $Extends = ['\RainLab\Pages\Classes\Page', 'Indikator\Content\Models\Portfolio', 'Indikator\Content\Models\News', 'Indikator\Content\Models\Blog','Backend\Classes\Controller'];
        foreach($Extends as $Extend)
        {
            $this->addMethods($Extend);
        }
        $this->registerConsoleCommand('greymen.formsco.importacfields', 'Greymen\Formsco\Console\ImportAcFields');

    }

    public function addMethods($class)
    {
        if( ! class_exists($class) ) return;
        $class::extend(function($model) {
            $model->addDynamicMethod('getPageOptions', function() {
                $theme = Theme::getEditTheme();
                $pageList = new PageList($theme);
                $pages = [];
                foreach ($pageList->listPages() as $name => $pageObject) {
                    $pages[$pageObject->url] = $pageObject->title . ' (' . $pageObject->url . ')';
                }
                return $pages;
            });
            $model->addDynamicMethod('getListOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getLists();
            });
            $model->addDynamicMethod('getAcFieldNameOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getFields();
            });
            $model->addDynamicMethod('getFormIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });
            $model->addDynamicMethod('getCampaignIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });

            $model->addDynamicMethod('onRefreshFields', function() {
                Artisan::call('formsco:importacfields');
                Flash::success('Fields updated');
            });
        });
    }

    public function register()
    {
        $this->registerValidationRule('google_recaptcha', reCAPTCHA::class);
    }

    public function registerComponents()
    {
        return
        [
            "Greymen\Formsco\Components\UtmSession"    => "utmSession",
            'Greymen\Formsco\Components\Contact'       => 'Contact',
            'Greymen\Formsco\Components\Newsletter'    => 'Newsletter',
            'Greymen\Formsco\Components\Form'          => 'Form',
        ];
    }
    public function registerMailTemplates()
    {
        return [
            'greymen.formsco::mail.subscribed',
        ];
    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label' => 'greymen.formsco::lang.plugin.name',
                'category' => 'system::lang.system.categories.cms',
                'icon' => 'oc-icon-user',
                'description' => 'greymen.formsco::lang.plugin.description',
                'class' => 'Greymen\Formsco\Models\Settings',
                'order' => 800,
            ]
        ];
    }
    public function registerReportWidgets()
    {
        return [
            'Greymen\Formsco\ReportWidgets\SubscriberStats' => [
                'label'   => 'Subscribers stats',
                'context' => 'dashboard'
            ]
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'getFormSettings' => function ($name, $default = '') {
                    return Settings::get($name, $default);
                },
            ],
        ];
    }


}
