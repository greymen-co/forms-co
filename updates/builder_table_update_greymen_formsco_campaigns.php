<?php namespace Greymen\Formsco\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenFormscoCampaigns extends Migration
{
    public function up()
    {
        Schema::table('greymen_formsco_campaigns', function($table)
        {
            $table->boolean('enable_captcha')->after('send_data_to_zapier')->nullable()->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('greymen_formsco_campaigns', function($table)
        {
            $table->dropColumn('enable_captcha');
        });
    }
}
