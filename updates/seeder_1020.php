<?php namespace Greymen\Formsco\Updates;

use Seeder;
use Greymen\Formsco\Models\Form;
class Seeder1020 extends Seeder
{
    public function run()
    {
        $form = Form::create([
            'shortcode' => 'utm',
            'name' => 'UTM field mapper',
            'file' => 'Form::utm'
        ]);
    }
}