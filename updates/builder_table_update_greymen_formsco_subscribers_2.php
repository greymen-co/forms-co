<?php namespace Greymen\Formsco\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenFormscoSubscribers2 extends Migration
{
    public function up()
    {
        Schema::table('greymen_formsco_subscribers', function($table)
        {
            $table->text('utm')->nullable()->unsigned(false)->default(null)->comment(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_formsco_subscribers', function($table)
        {
            $table->string('utm', 255)->nullable()->unsigned(false)->default(null)->comment(null)->change();
        });
    }
}