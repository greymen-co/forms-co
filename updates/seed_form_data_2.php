<?php namespace Greymen\Formsco\Updates;

use Db;
use Seeder;

class SeedFormData extends Seeder
{
    public function run()
    {
        $user = Db::table('greymen_formsco_form')->insert([[
            'shortcode'    => 'hidden',
            'name'         => 'Hidden field',
            'file'         => 'Form::hidden'
        ]]
        );
    }
}
