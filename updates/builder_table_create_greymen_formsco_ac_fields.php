<?php namespace Greymen\Formsco\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGreymenFormscoAcFields extends Migration
{
    public function up()
    {
        Schema::create('greymen_formsco_ac_fields', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title', 120)->nullable();
            $table->text('descript')->nullable();
            $table->string('type', 100)->nullable();
            $table->string('isrequired', 10)->nullable();
            $table->string('perstag', 100)->nullable();
            $table->string('links_options', 200)->nullable();
            $table->string('links_relations', 200)->nullable();
            $table->integer('ac_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('greymen_formsco_ac_fields');
    }
}
