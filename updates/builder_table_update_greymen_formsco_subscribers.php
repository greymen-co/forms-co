<?php namespace Greymen\Formsco\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenFormscoSubscribers extends Migration
{
    public function up()
    {
        Schema::table('greymen_formsco_subscribers', function($table)
        {
            $table->text('files')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_formsco_subscribers', function($table)
        {
            $table->dropColumn('files');
        });
    }
}
