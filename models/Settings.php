<?php
namespace Greymen\Formsco\Models;

use Model;

/**
 * Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'greymen_formsco_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
