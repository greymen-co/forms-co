<?php namespace Greymen\Formsco\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Greymen\Formsco\Models\AcField;
use Greymen\Formsco\Classes\ActiveCampaign;

/**
 * ImportAcFields Command
 */
class ImportAcFields extends Command
{
    /**
     * @var string name is the console command name
     */
    protected $name = 'formsco:importacfields';

    /**
     * @var string description is the console command description
     */
    protected $description = 'Import all AC fields for caching';

    /**
     * handle executes the console command
     */
    public function handle()
    {
        $acf =  new AcField;
        $acf->truncate();
        $ac = new ActiveCampaign;
        $response =  $ac->getAllFields();
        $data = [
                    [
                        'id'        => 1,
                        'title'     => 'EMAIL',
                        'type'      => 'text',
                        'perstag'   => 'EMAIL',
                        'ac_id'     => 1
                    ],[
                        'id'        => 2,
                        'title'     => 'PHONE',
                        'type'      => 'text',
                        'perstag'   => 'PHONE',
                        'ac_id'     => 2
                    ],[
                        'id'        => 3,
                        'title'     => 'LASTNAME',
                        'type'      => 'text',
                        'perstag'   => 'LASTNAME',
                        'ac_id'     => 3
                    ],[
                        'id'        => 4,
                        'title'     => 'FIRSTNAME',
                        'type'      => 'text',
                        'perstag'   => 'FIRSTNAME',
                        'ac_id'     => 4
                    ]
                ];
        $acf->insert($data);
        foreach(@$response as $field)
        {
            if (isset($field->id)) {

                $acf->create (
                    [
                        'title'             => @$field->title,
                        'descript'          => @$field->descript,
                        'type'              => @$field->type,
                        'isrequired'        => @$field->isrequired,
                        'perstag'           => @$field->perstag,
                        'links_options'     => @$field->links->options,
                        'links_relations'   => @$field->links->relations,
                        'ac_id'             => @$field->id
                    ]
                );
            }
        }

    }

    /**
     * getArguments get the console command arguments
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * getOptions get the console command options
     */
    protected function getOptions()
    {
        return [];
    }
}
