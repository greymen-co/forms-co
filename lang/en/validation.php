<?php
return [
    'fullname.required' => 'Je bent vergeten je naam in te vullen',
    'email.required' => 'Je bent vergeten je email in te vullen',
    'email.email' => 'Je bent vergeten je email in te vullen',
    'g-recaptcha-response.required' => 'The reCAPTCHA is required.',
    'recaptcha' => [
        'failed' => 'Google reCAPTCHA was unable to verify the form.',
    ],
];
